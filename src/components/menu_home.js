import React, { Component } from 'react';


const MenuHome = () => {
    return (
        <div> 
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="col-1"><a className="navbar-brand" href="#">Sobre</a></div>
                <div className="col-1"><a className="navbar-brand" href="#">Equipe</a></div>    
                <div className="col-2"><a className="navbar-brand" href="#">Exames Suportados</a></div>    
                <div className="col-1"><a className="navbar-brand" href="#">Contato</a></div>    
                <div className="col-3"><img className="img imgtam" src="../images/logohome.png" /> </div> 
                <div className="col-1"></div>   
                <div className="col-1"><button type="button" className="btn btn-success btn-lg">Entrar</button></div>    
                <div className="col-1"><button type="button" className="btn btn-info btn-lg">Registrar</button> </div>
            </nav>
        </div>
        );
}


export default MenuHome;