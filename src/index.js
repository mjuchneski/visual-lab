import React, {Component} from 'react';
import ReactDom from 'react-dom';

import Language from './components/language_home';
import MenuHome from './components/menu_home';
import ImagesHome from './components/images_home';
import DescricaoHome from './components/descricao_home';



class App extends Component { 

  render() {

    return (
        <div>
           <Language />
           <MenuHome />
           <ImagesHome />
           <DescricaoHome />

        </div>
    ); 
  }
}

ReactDom.render(<App />, document.querySelector('.cont'));