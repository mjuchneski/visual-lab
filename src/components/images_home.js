import React, { Component } from 'react';

const ImagesHome = () => {
        return (
            <div>
                <div className="col-7" >
                    <img className="imgResponsive" src="../images/01.png" />
                </div>
                <div className="col-5" >
                    <h2>Imagens térmicas</h2>
                    <br></br>
                    <p>Termografia é um exame fisiológico, não invasivo e sem uso de radiações ionizantes. Possibilita a detecção de tumores mamários, muito antes que qualquer outro método, ainda quando as células produzem substâncias responsáveis pela criação de neovascularização que alimentará o futuro tumor. </p>
                </div>
                <div className="col-12"></div>
                <div className="col-5">
                    <h2>Banco de dados de imagens mastológicas</h2>
                    <br></br>
                    <p>DMR - Database For Mastology Research é uma plataforma online que armazena e gerencia imagens mastológicas, para a detecção precoce de câncer de mama. Aqui são disponibilizados imagens térmicas, mamografias, ressonância magnética e imagens de ultrasom obtidas por nosso grupo de pesquisa.</p>
                </div>
                <div className="col-7">
                    <img className="imgResponsive" src="../images/detalhe1.png" />
                </div>
            </div>
        );
    }


export default ImagesHome;


