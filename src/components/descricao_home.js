import React, { Component } from 'react';

const DescricaoHome = () => {
        return (

            <div className="col-12">
                 <h3>Processamento e Análise de Imagens Aplicadas a Mastologia</h3>
                <div className="col-8">
                   
                    <p>
                        Os novos equipamentos digitais para aquisição de imagens médicas(câmeras termográficas, ultra-som,
                        mamogramas digitais, etc) nos permitem combinar as informações anatômicas das diversas fontes
                        para as específicidades dos pacientes. Estes dados (imagens) devem ser processados para realçar
                        e extrair características. Nesta área, técnicas de processamento de imagens e reconhecimento
                        de padrões são muito importantes, tanto para automatizar certos procedimentos, quanto para
                        facilitar a interação e combinação dos dados.
                    </p>
                    <p>
                        A proposta desta pesquisa é auxiliar no diagnóstico por imagens médicas através da
                        integração das principais etapas do processamento de imagens aos métodos de extração
                        de características e conhecimento, visando a interpreção das imagens, a identificação
                        de tecidos e estruturas anatômicas e funcionais. Neste sentido, buscamos conduzir as
                        pesquisas ao problema de auxiliar nos diagnósticos precoces e na classificação de
                        patologias(benignas/malignas) da mama. O projeto vai desde a aquisição das imagens
                        e sua organização em bancos de dados, passando pela suas análises através de 
                        métodos numéricos, até um último estágio, onde técnicas de aprendizado de máquina
                        devem ser utilizadas de forma a ser feita uma extração de conhecimento das
                        Imagens de Mamas.
                    </p>
                    <p>
                        Câmera usada no projeto FLIR SC-620, 
                    </p>
                    <p>
                        Resolução 640 x 480: Pixel = 45 μm
                    </p>
                        
                    
                </div>
            </div>

        );
}



export default DescricaoHome;